% Model autoimmune disease
function dYdt = autoim(t,Y)

H=Y(1); 
D=Y(2);
E=Y(3); 
V=Y(4);


p11 = 0.5;
d11 = 0.2;
d13 = 0.1; 
d22 = 0.8;
p32 = 0.0004;
d33 = 0.1;
c3 = 23; 
p34 = 0.4; %not found in the article
d43 = 4;
p4 = 23;
n=1;
m=1;
c=1;
u = 1;
v = 1;

dY1dt = p11*Y(1) - d13*Y(1)*Y(3) -d11*Y(1);
dY2dt = d13*Y(1)*Y(3)-d22*Y(2);
dY3dt = p32*Y(2)+p34*Y(4)- d33*Y(3) + c3*Y(3); % Basic

% With g(E)
%dY3dt = p32*Y(2)+p34*Y(4)- d33*Y(3) + c3*(((Y(3))^n) / (c^n + (Y(3))^n));

% With g(E) + f(D) + f(V)
%dY3dt = p32*((((Y(2))^u)/(m^v +(Y(2))^v)))+p34*((((Y(4))^u)/(m^v +(Y(4))^v)))- d33*Y(3) + c3*(((Y(3))^n) / (c^n + (Y(3))^n));
dY4dt = p4*Y(4) - d43*Y(4)*Y(3);

dYdt = [dY1dt,dY2dt, dY3dt, dY4dt]'

end