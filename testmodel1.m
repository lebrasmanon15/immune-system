% Test model 1
tRange = [0 0.5];
Y0 = [5;180];

[tSol,YSol] = ode45(@model1,tRange,Y0);

E = YSol(:,1);
T = YSol(:,2);


plot(tSol,E,"c",tSol,T,"b","LineWidth",1.5);

legend('E','T');
xlabel("Time (hours)");
ylabel("Population size");
title("Model 1"); 