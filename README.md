# Immune system

Modeling project in Matlab for Master 1 final Project (in group of 2)


## Name
How complex the immune system model has to be at least in order to produce its different
behaviors?

## Description
In this Master 1 Modeling final project we explore different way to model the immune system dynamics. Inspired by the litterature we develop basic mathematical model to more complex ones.

[1] Mayer, H., Zaenker, K. S., & An Der Heiden, U. (1995). A basic mathematical model of the immune response. Chaos: An Interdisciplinary Journal of Nonlinear Science, 5(1), 155-161.


## Installation
We used Matlab (online version) to develop this project. 


## Authors and acknowledgment
TeamWork of 2 Master1 students
Randrianirina Manon & SI

