function dx = model_hiv2(t, x)


s=250;
d = 0.1;
r = 0.1;
x_max = 1000;
b = 5;
i = 0.1;
c = 0.3;
p=0.1;
k=0.002;
a=5; 
N =10;
tho = 0.5;
w = 0.1;
alpha = 2; 
c=1;
n=3;
omega = 1;
u =2;
v=2;
m=1;

dx1 = s - d*x(1)- k*x(1)*x(3) + r*x(1)*(1 - x(1)/x_max);
dx2 = k*x(1)*x(3) - b*x(2)- p*x(2)*x(4);
dx3 = N*b*x(2) - a*x(3);
%dx4 = c*x(2) + w*x(4)- i*x(4); %simple case
%dx4 = c*x(2) + alpha*((x(4)^n) / (c^n + x(4)^n))- i*x(4); %mi-complex case
%dx4 = omega*((x(2)^u)/(m^v+ x(2)^v)) + alpha*((x(4)^n) / (c^n + x(4)^n))- i*x(4); %complex case

dx4 = c*x(2)*(t - tho) + alpha*((x(4)^n) / (c^n + x(4)^n)) - i*x(4);%pour voir le time delay

dx = [dx1; dx2; dx3; dx4];
end