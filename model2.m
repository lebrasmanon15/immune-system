% Model 2
function dYdt = model2(t,Y)

E=Y(1); 
T=Y(2);

a=2.5;
b=0.1;
r = 7.8;



dY1dt = a*Y(2);
dY2dt = -b*Y(1)*Y(2) + r*Y(2);


dYdt = [dY1dt,dY2dt]'

end