% Model 3
function dYdt = model3(t,Y)

E=Y(1); 
T=Y(2);

a=2.5;
b=0.1; 
r = 7.8;
w = 1.2;



dY1dt = a*Y(2) + w*Y(1);
dY2dt = -b*Y(1)*Y(2) + r*Y(2);


dYdt = [dY1dt,dY2dt]'

end