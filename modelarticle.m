% Model article complex mathematical model
function dYdt = modelarticle(t,Y)

E=Y(1); 
T=Y(2);

%Case 1
p=1;
b=2; 
r = 2.3;
s = 2.5;
d = 1;
u = 1;
v = 1;
n=1;
m=1;
c=1;


%fT = articlefT(Y(2)) 

dY1dt = p*((((Y(2))^u)/(m^v +(Y(2))^v))) + s*(((Y(1))^n) / (c^n + (Y(1))^n)) - d*Y(1) ;
dY2dt = -b*Y(1)*Y(2) + r*Y(2);


dYdt = [dY1dt,dY2dt]'

end




% Case 2
% 
% a=0.7;
% b=0.1; 
% g = 0.15;
% d = 2;
% i = 0.1;
% n=3;
% c=1;

%Case 3
% b=1; 
% g = 1.2;
% d = 2;
% i = 1;
% n=3;
% c=1;
% a=0.28;
% u = 2;
% v = 2;
% m=1;