%Parameter estimation for loop

function dy = model_1(t, y)
  dy = [0;0];
  global a;
  b=0.1;
  dy(1) = a*y(2); %E
  dy(2) = -b*y(1)*y(2); %T
end
